package com.wordpress.alexpihldevblog.deipneus.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class IngredientVariant
{
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
    @JoinColumn(name = "ingredient_id")
	private Ingredient ingredient;
	
	private float quantity;
	
	@ManyToOne
    @JoinColumn(name = "measure_unit_id")
	private MeasureUnit unit;
	
	public IngredientVariant()
	{ }
	
	public void setId(long value)
	{
		id = value;
	}
	
	public void setIngredient(long id)
	{
		ingredient = new Ingredient(id);
	}
	
	public void setQuantity(float value)
	{
		quantity = value;
	}
	
	public void setUnit(MeasureUnit value)
	{
		unit = value;
	}
	
	public MeasureUnit getUnit()
	{
		return unit;
	}
	
	public float getQuantity()
	{
		return quantity;
	}
	
	public Ingredient getIngredient()
	{
		return ingredient;
	}
	
	public long getId()
	{
		return id;
	}
}
