package com.wordpress.alexpihldevblog.deipneus.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wordpress.alexpihldevblog.deipneus.persistence.model.MeasureUnit;

public interface MeasureUnitRepository extends JpaRepository<MeasureUnit, Long>
{
}
