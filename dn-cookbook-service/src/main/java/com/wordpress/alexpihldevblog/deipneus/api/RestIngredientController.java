package com.wordpress.alexpihldevblog.deipneus.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wordpress.alexpihldevblog.deipneus.dto.IngredientDto;
import com.wordpress.alexpihldevblog.deipneus.dto.IngredientForm;
import com.wordpress.alexpihldevblog.deipneus.service.IngredientService;

@RestController
@RequestMapping(
		path = "/api/1.0",
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
public class RestIngredientController
{
	
	@Autowired
	private IngredientService service;
	
	@RequestMapping(value = "/ingredients", method = RequestMethod.GET)
	private ResponseEntity<String> getAllIngredients()
	{
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(value = "/ingredients", method = RequestMethod.POST)
	private ResponseEntity<?> createIngredient(@Valid @RequestBody final IngredientForm form, final Errors errors)
	{
		if (errors.hasErrors())
		{
			return ResponseEntity.badRequest().build();
		}
		
		IngredientDto result = new IngredientDto();
		result.setName(form.getName());
		
		result = service.createNew(result);
		if (result == null)
		{
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		
		return ResponseEntity.ok(result);
	}
}
