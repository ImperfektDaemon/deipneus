package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.List;

import com.wordpress.alexpihldevblog.deipneus.dto.IngredientDto;

public interface IngredientService
{
	IngredientDto createNew(IngredientDto ingredient);
	
	IngredientDto fetchOne(long id);
	
	List<IngredientDto> fetchAll();
}
