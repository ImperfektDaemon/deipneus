package com.wordpress.alexpihldevblog.deipneus.web;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wordpress.alexpihldevblog.deipneus.dto.IngredientVariantDto;
import com.wordpress.alexpihldevblog.deipneus.dto.InstructionDto;
import com.wordpress.alexpihldevblog.deipneus.dto.RecipeDto;
import com.wordpress.alexpihldevblog.deipneus.dto.RecipeForm;
import com.wordpress.alexpihldevblog.deipneus.exception.ResourceNotFoundException;
import com.wordpress.alexpihldevblog.deipneus.service.IngredientService;
import com.wordpress.alexpihldevblog.deipneus.service.MeasureUnitService;
import com.wordpress.alexpihldevblog.deipneus.service.RecipeService;

@Controller
public class WebRecipeController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(WebRecipeController.class);
	
	@Autowired
	private RecipeService recipeService;
	
	@Autowired
	private MeasureUnitService unitService;
	
	@Autowired
	private IngredientService ingredientService;
	
	@RequestMapping(value = "/recipes", method = RequestMethod.GET)
	private ModelAndView getAllRecipes(final ServletRequest request)
	{
		final ModelAndView mav = new ModelAndView("recipe/recipe-list");
		mav.addObject("new_editor_link", String.format("%s/recipes/editor", getBaseUrl(request)));
		
		return mav;
	}
	
	@RequestMapping(value = "/recipes/editor", method = RequestMethod.GET)
	private ModelAndView getRecipeEditor(final ServletRequest request)
	{
		return prepareRecipeFormView(recipeService.createRecipe());
	}
	
	@RequestMapping(value = "/recipes/{id}", method = RequestMethod.GET)
	private ModelAndView getRecipe(@PathVariable("id") final Long id, final ServletRequest request)
	{
		final RecipeDto recipe = recipeService.fetchOne(id);
		if (recipe == null)
		{
			throw new ResourceNotFoundException();
		}
		
		return prepareRecipeView(request, recipe);
	}
	
	@RequestMapping(value = "/recipes/{id}/editor", method = RequestMethod.GET)
	private ModelAndView getRecipeEditor(@PathVariable("id") final Long id)
	{
		final RecipeDto recipe = recipeService.fetchOne(id);
		if (recipe == null)
		{
			throw new ResourceNotFoundException();
		}
		
		return prepareRecipeFormView(recipe);
	}
	
	@RequestMapping(value = "/recipes", method = RequestMethod.POST)
	private ModelAndView postRecipeEditor(@Valid final RecipeForm form, final Errors errors, final ServletRequest request)
	{
		final RecipeDto recipe = recipeService.fetchOne(form.getId());
		if (recipe == null)
		{
			throw new ResourceNotFoundException();
		}
		
		if (errors.hasErrors())
		{
			LOGGER.warn(errors.getAllErrors().toString());
			
			return prepareRecipeFormView(recipe, errors.getFieldErrors()
					.stream()
					.map(err -> String.format("%s: %s"
							, err.getField()
							, err.getDefaultMessage()))
					.collect(Collectors.toList()));
		}
		
		recipe.setName(form.getName());
		recipe.setServes(form.getServes());
		recipe.setPrepTime(LocalTime.parse(form.getPrepTime()));
		recipe.setCookingTime(LocalTime.parse(form.getCookingTime()));
		recipe.setDescription(form.getDescription());
		recipe.setVariants(form.getVariants()
				.stream()
				.map(f -> new IngredientVariantDto(
							f.getId(),
							f.getIngredientId(),
							f.getIngredientName(),
							f.getQuantity(),
							f.getUnitId()
						))
				.collect(Collectors.toList()));
		recipe.setInstructions(form.getInstructions()
				.stream()
				.map(f -> new InstructionDto(f.getText()))
				.collect(Collectors.toList()));
		
		recipeService.updateRecipe(recipe);
		
		return new ModelAndView(String.format("redirect:%s/recipes/%d", getBaseUrl(request), form.getId()));
	}
	
	
	private ModelAndView prepareRecipeView(final ServletRequest request, final RecipeDto recipe)
	{
		final ModelAndView mav = new ModelAndView("recipe/recipe");
		mav.addObject("editor_link", String.format("%s/recipes/%d/editor", getBaseUrl(request), recipe.getId()));
		mav.addObject("recipe", recipe);
		
		return mav;
	}
	
	private ModelAndView prepareRecipeFormView(final RecipeDto recipe)
	{
		return prepareRecipeFormView(recipe, new ArrayList<>());
	}
	
	private ModelAndView prepareRecipeFormView(final RecipeDto recipe, final List<String> errors)
	{
		final ModelAndView mav = new ModelAndView("recipe/recipe-editor");
		mav.addObject("recipe", recipe);
		mav.addObject("errors", errors);
		mav.addObject("ingredients", ingredientService.fetchAll()); // @HACK: hard-coding all ingredients existing in the database for now.
		mav.addObject("units", unitService.fetchAll());
		
		return mav;
	}
	
	private String getBaseUrl(final ServletRequest request)
	{
		// @TODO: Refactor server host & port. Hard-coded for now because we do NOT want to return the microservice port.
		final ServletContext ctx = request.getServletContext();
		return String.format("%s://%s:%d%s",request.getScheme(), request.getServerName(), 8765, ctx.getContextPath());
		//return String.format("%s://%s:%d/%s",request.getScheme(), request.getServerName(), request.getServerPort(), ctx.getContextPath());
	}
}
