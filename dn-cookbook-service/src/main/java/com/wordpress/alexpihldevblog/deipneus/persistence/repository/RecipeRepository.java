package com.wordpress.alexpihldevblog.deipneus.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wordpress.alexpihldevblog.deipneus.persistence.model.Recipe;

public interface RecipeRepository extends JpaRepository<Recipe, Long>
{
}
