package com.wordpress.alexpihldevblog.deipneus.dto;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class RecipeForm
{
	
	@NotNull
	@Positive
	private long id;
	
	@NotNull
	@Size(min = 3, max = 40)
	private String name;
	
	@NotNull
	@Min(1)
	@Max(99)
	private short serves;
	
	@NotNull
	@Pattern(regexp = "[0-9]{2}:[0-9]{2}")
	private String prepTime;
	
	@NotNull
	@Pattern(regexp = "[0-9]{2}:[0-9]{2}")
	private String cookingTime;
	
	@NotNull
	@Size(max = 400)
	private String description;
	
	@NotNull
	private List<IngredientVariantForm> variants;
	
	@NotNull
	private List<InstructionForm> instructions;
	
	public void setId(long value)
	{
		id = value;
	}
	
	public void setName(String value)
	{
		name = value;
	}
	
	public void setServes(short value)
	{
		serves = value;
	}
	
	public void setPrepTime(String value)
	{
		prepTime = value;
	}
	
	public void setCookingTime(String value)
	{
		cookingTime = value;
	}
	
	public void setVariants(List<IngredientVariantForm> value)
	{
		variants = value;
	}
	
	public void setInstructions(List<InstructionForm> value)
	{
		instructions = value;
	}
	
	public void setDescription(String value)
	{
		description = value;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public List<InstructionForm> getInstructions()
	{
		return instructions;
	}
	
	public List<IngredientVariantForm> getVariants()
	{
		return variants;
	}
	
	public String getCookingTime()
	{
		return cookingTime;
	}
	
	public String getPrepTime()
	{
		return prepTime;
	}
	
	public short getServes()
	{
		return serves;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getId()
	{
		return id;
	}
}
