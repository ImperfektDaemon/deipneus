package com.wordpress.alexpihldevblog.deipneus.dto;

import javax.validation.constraints.Size;

public class IngredientForm
{
	
	@Size(min = 2, max = 20)
	private String name;
	
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}
}
