package com.wordpress.alexpihldevblog.deipneus.dto;

public class IngredientDto
{
	
	private long id;
	
	private String name;
	
	public IngredientDto()
	{
		this(0);
	}
	
	public IngredientDto(long id)
	{
		this(id, "");
	}
	
	public IngredientDto(long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getId()
	{
		return id;
	}
}
