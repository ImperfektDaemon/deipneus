package com.wordpress.alexpihldevblog.deipneus.persistence.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

@Entity
public class MeasureUnit
{
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	@Size(max = 20)
	private String fullName;
	
	@Column(nullable = false)
	@Size(max = 5)
	private String shortName;
	
	@OneToMany(mappedBy = "unit", cascade = CascadeType.ALL)
	private Set<IngredientVariant> ingredients;
	
	public MeasureUnit()
	{
		this(0);
	}
	
	public MeasureUnit(long id)
	{
		this.id = id;
	}
	
	public String getShortName()
	{
		return shortName;
	}
	
	public String getFullName()
	{
		return fullName;
	}
	
	public long getId()
	{
		return id;
	}
}
