package com.wordpress.alexpihldevblog.deipneus.persistence.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "ingredient")
public class Ingredient
{
	
	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	@NotBlank(message = "name is required")
	private String name;
	
	@OneToMany(mappedBy = "ingredient", cascade = CascadeType.ALL)
	private Set<IngredientVariant> variants;
	
	public Ingredient()
	{
		this(0);
	}
	
	public Ingredient(long id)
	{
		this.id = id;
	}
	
	public void setName(String value)
	{
		name = value;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getId()
	{
		return id;
	}
}
