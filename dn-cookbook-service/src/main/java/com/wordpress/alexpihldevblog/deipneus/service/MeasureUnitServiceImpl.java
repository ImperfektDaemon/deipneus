package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wordpress.alexpihldevblog.deipneus.dto.MeasureUnitDto;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.MeasureUnit;
import com.wordpress.alexpihldevblog.deipneus.persistence.repository.MeasureUnitRepository;

@Service
public class MeasureUnitServiceImpl implements MeasureUnitService
{

	@Autowired
	private MeasureUnitRepository repository;
	
	@Override
	public List<MeasureUnitDto> fetchAll()
	{
		final List<MeasureUnitDto> result = new ArrayList<>();
		final List<MeasureUnit> entities = repository.findAll();
		for (MeasureUnit entity : entities)
		{
			result.add(fetch(entity));
		}
		
		return result;
	}
	
	
	private MeasureUnitDto fetch(final MeasureUnit entity)
	{
		return new MeasureUnitDto(entity.getId()
				, entity.getFullName()
				, entity.getShortName());
	}
}
