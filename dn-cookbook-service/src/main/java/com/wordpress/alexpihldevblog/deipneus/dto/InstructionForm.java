package com.wordpress.alexpihldevblog.deipneus.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class InstructionForm
{
	
	@NotNull
	@Size(min = 3, max = 40)
	private String text;
	
	public void setText(String value)
	{
		text = value;
	}
	
	public String getText()
	{
		return text;
	}
}
