package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.List;

import com.wordpress.alexpihldevblog.deipneus.dto.MeasureUnitDto;

public interface MeasureUnitService
{
	List<MeasureUnitDto> fetchAll();
}
