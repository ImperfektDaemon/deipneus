package com.wordpress.alexpihldevblog.deipneus.dto;

public class MeasureUnitDto
{
	
	private long id;
	
	private String fullName;
	
	private String shortName;
	
	public MeasureUnitDto(long id, String fullName, String shortName)
	{
		this.id = id;
		this.fullName = fullName;
		this.shortName = shortName;
	}
	
	public String getShortName()
	{
		return shortName;
	}
	
	public String getFullName()
	{
		return fullName;
	}
	
	public long getId()
	{
		return id;
	}
}
