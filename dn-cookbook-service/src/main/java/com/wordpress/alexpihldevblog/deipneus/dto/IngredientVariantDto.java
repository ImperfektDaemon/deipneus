package com.wordpress.alexpihldevblog.deipneus.dto;

import com.wordpress.alexpihldevblog.deipneus.persistence.model.MeasureUnit;

public class IngredientVariantDto
{
	
	private long id;
	
	private IngredientDto ingredient;
	
	private float quantity;
	
	private MeasureUnit unit;

	public IngredientVariantDto()
	{
		this(0);
	}
	
	public IngredientVariantDto(long id)
	{
		this(id, -1, "", 0.0f, -1);
	}
	
	public IngredientVariantDto(long id, long ingredientId, String ingredientName, float quantity, long unitId)
	{
		this.id = id;
		this.ingredient = new IngredientDto(ingredientId, ingredientName);
		this.quantity = quantity;
		this.unit = new MeasureUnit(unitId);
	}
	
	public void setIngredient(IngredientDto value)
	{
		ingredient = value;
	}
	
	public void setQuantity(float value)
	{
		quantity = value;
	}
	
	public void setUnit(MeasureUnit value)
	{
		unit = value;
	}
	
	public MeasureUnit getUnit()
	{
		return unit;
	}
	
	public float getQuantity()
	{
		return quantity;
	}
	
	public IngredientDto getIngredient()
	{
		return ingredient;
	}
	
	public long getId()
	{
		return id;
	}
}
