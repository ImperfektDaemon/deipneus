package com.wordpress.alexpihldevblog.deipneus.persistence.model;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "recipe")
public class Recipe
{

	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	@NotBlank(message = "name is required")
	private String name;
	
	@Column(nullable = false)
	private short serves;
	
	@Column(nullable = false)
	private short prepTime;
	
	@Column(nullable = false)
	private short cookingTime;
	
	@Column(nullable = false)
	@NotBlank(message = "description is required")
	@Size(max = 400)
	private String description;
	
	@Column(nullable = false)
	@ElementCollection(targetClass = IngredientVariant.class)
	private Set<IngredientVariant> ingredients = new HashSet<>();
	
	@Column(nullable = false)
	@ElementCollection(targetClass = String.class)
	private Set<String> instructions = new HashSet<>();
	
	public Recipe()
	{ }
	
	public void setName(String value)
	{
		name = value;
	}
	
	public void setServes(short value)
	{
		serves = value;
	}
	
	public void setPrepTime(final LocalTime time)
	{
		prepTime = (short)(time.toSecondOfDay() / 60);
	}
	
	public void setCookingTime(final LocalTime time)
	{
		cookingTime = (short)(time.toSecondOfDay() / 60);
	}
	
	public void setDescription(String value)
	{
		description = value;
	}
	
	public void setIngredients(Set<IngredientVariant> value)
	{
		ingredients = value;
	}
	
	public void setInstructions(Set<String> value)
	{
		instructions = value;
	}
	
	public Set<String> getInstructions()
	{
		return instructions;
	}
	
	public Set<IngredientVariant> getIngredients()
	{
		return ingredients;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public LocalTime getCookingTime()
	{
		return LocalTime.of(cookingTime / 60, cookingTime % 60);
	}
	
	public LocalTime getPrepTime()
	{
		return LocalTime.of(prepTime / 60, prepTime % 60);
	}
	
	public short getServes()
	{
		return serves;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getId()
	{
		return id;
	}
}
