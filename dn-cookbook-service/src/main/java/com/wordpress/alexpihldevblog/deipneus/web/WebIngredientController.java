package com.wordpress.alexpihldevblog.deipneus.web;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wordpress.alexpihldevblog.deipneus.service.IngredientService;

@Controller
public class WebIngredientController
{
	
	@Autowired
	private IngredientService service;
	
	@RequestMapping(value = "/ingredients", method = RequestMethod.GET)
	private ModelAndView getAllRecipes(final ServletRequest request)
	{
		final ModelAndView mav = new ModelAndView("ingredient/ingredient-list");
		mav.addObject("ingredients", service.fetchAll());
		
		return mav;
	}
}
