package com.wordpress.alexpihldevblog.deipneus.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wordpress.alexpihldevblog.deipneus.dto.RecipeDto;
import com.wordpress.alexpihldevblog.deipneus.service.RecipeService;

@RestController
@RequestMapping(
		path = "/api/1.0",
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
public class RestRecipeController
{
	
	@Autowired
	private RecipeService service;
	
	@RequestMapping(value = "/recipes", method = RequestMethod.GET)
	private ResponseEntity<List<RecipeDto>> getAllRecipes()
	{
		final List<RecipeDto> recipes = service.fetchAll();
		return ResponseEntity.ok(recipes);
	}
	
	@RequestMapping(value = "/recipes/{id}", method = RequestMethod.DELETE)
	private ResponseEntity<String> deleteRecipe(@PathVariable("id") final long id)
	{
		if (!service.deleteRecipe(id))
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok("{}");
	}
}
