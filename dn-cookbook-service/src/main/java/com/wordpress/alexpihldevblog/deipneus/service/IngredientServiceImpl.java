package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wordpress.alexpihldevblog.deipneus.dto.IngredientDto;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.Ingredient;
import com.wordpress.alexpihldevblog.deipneus.persistence.repository.IngredientRepository;

@Service
@Transactional
public class IngredientServiceImpl implements IngredientService
{
	
	@Autowired
	private IngredientRepository repository;
	
	@Override
	public IngredientDto createNew(final IngredientDto ingredient)
	{
		final Ingredient entity = new Ingredient();
		entity.setName(ingredient.getName());
		
		return fetch(repository.save(entity));
	}
	
	@Override
	public IngredientDto fetchOne(long id)
	{
		final Optional<Ingredient> opt = repository.findById(id);
		if (!opt.isPresent())
		{
			return null;
		}
		
		return fetch(opt.get());
	}
	
	@Override
	public List<IngredientDto> fetchAll()
	{
		final List<IngredientDto> result = new ArrayList<>();
		final List<Ingredient> entities = repository.findAll();
		for (Ingredient entity : entities)
		{
			result.add(fetch(entity));
		}
		
		return result;
	}
	
	
	private IngredientDto fetch(final Ingredient entity)
	{
		final IngredientDto result = new IngredientDto(entity.getId());
		result.setName(entity.getName());
		
		return result;
	}
}
