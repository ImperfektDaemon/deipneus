package com.wordpress.alexpihldevblog.deipneus.dto;

import java.time.LocalTime;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class RecipeDto
{
	private long id;
	
	private String name;
	
	private short serves;
	
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime prepTime;
	
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime cookingTime;
	
	private String description;
	
	private List<IngredientVariantDto> variants;
	
	private List<InstructionDto> instructions;
	
	public RecipeDto(long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public void setName(String value)
	{
		name = value;
	}
	
	public void setServes(short value)
	{
		serves = value;
	}
	
	public void setPrepTime(final LocalTime value)
	{
		prepTime = value;
	}
	
	public void setCookingTime(final LocalTime value)
	{
		cookingTime = value;
	}
	
	public void setDescription(String value)
	{
		description = value;
	}
	
	public void setVariants(List<IngredientVariantDto> value)
	{
		variants = value;
	}
	
	public void setInstructions(List<InstructionDto> value)
	{
		instructions = value;
	}
	
	public List<InstructionDto> getInstructions()
	{
		return instructions;
	}
	
	public List<IngredientVariantDto> getVariants()
	{
		return variants;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public LocalTime getCookingTime()
	{
		return cookingTime;
	}
	
	public LocalTime getPrepTime()
	{
		return prepTime;
	}
	
	public short getServes()
	{
		return serves;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getId()
	{
		return id;
	}
}
