package com.wordpress.alexpihldevblog.deipneus.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wordpress.alexpihldevblog.deipneus.persistence.model.IngredientVariant;

public interface IngredientVariantRepository extends JpaRepository<IngredientVariant, Long>
{
}
