package com.wordpress.alexpihldevblog.deipneus.dto;

public class InstructionDto
{
	
	private String text;
	
	public InstructionDto(String text)
	{
		this.text = text;
	}
	
	public void setText(String value)
	{
		text = value;
	}
	
	public String getText()
	{
		return text;
	}
}
