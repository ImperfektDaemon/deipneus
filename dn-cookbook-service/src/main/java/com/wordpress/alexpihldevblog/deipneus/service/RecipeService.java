package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.List;

import com.wordpress.alexpihldevblog.deipneus.dto.RecipeDto;

public interface RecipeService
{
	RecipeDto createRecipe();
	
	void updateRecipe(RecipeDto dto);
	
	boolean deleteRecipe(long id);
	
	RecipeDto fetchOne(long id);
	
	List<RecipeDto> fetchAll();
}
