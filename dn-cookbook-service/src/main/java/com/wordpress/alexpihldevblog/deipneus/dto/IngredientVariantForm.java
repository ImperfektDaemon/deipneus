package com.wordpress.alexpihldevblog.deipneus.dto;

import javax.validation.constraints.Size;

public class IngredientVariantForm
{
	
	private long id;
	
	private long ingredientId;
	
	@Size(max = 20)
	private String ingredientName;
	
	private float quantity;
	
	private long unitId;
	
	public void setId(long value)
	{
		id = value;
	}
	
	public void setIngredientId(long value)
	{
		ingredientId = value;
	}
	
	public void setIngredientName(String value)
	{
		ingredientName = value;
	}
	
	public void setQuantity(float value)
	{
		quantity = value;
	}
	
	public void setUnitId(long value)
	{
		unitId = value;
	}
	
	public long getUnitId()
	{
		return unitId;
	}
	
	public float getQuantity()
	{
		return quantity;
	}
	
	public String getIngredientName()
	{
		return ingredientName;
	}
	
	public long getIngredientId()
	{
		return ingredientId;
	}
	
	public long getId()
	{
		return id;
	}
}
