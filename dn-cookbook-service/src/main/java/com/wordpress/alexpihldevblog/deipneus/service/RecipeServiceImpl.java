package com.wordpress.alexpihldevblog.deipneus.service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wordpress.alexpihldevblog.deipneus.dto.IngredientDto;
import com.wordpress.alexpihldevblog.deipneus.dto.IngredientVariantDto;
import com.wordpress.alexpihldevblog.deipneus.dto.InstructionDto;
import com.wordpress.alexpihldevblog.deipneus.dto.RecipeDto;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.Ingredient;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.IngredientVariant;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.Recipe;
import com.wordpress.alexpihldevblog.deipneus.persistence.repository.IngredientVariantRepository;
import com.wordpress.alexpihldevblog.deipneus.persistence.repository.RecipeRepository;

@Service
@Transactional
public class RecipeServiceImpl implements RecipeService
{
	
	@Autowired
	private RecipeRepository recipeRepository;
	
	@Autowired
	private IngredientService ingredientService;
	
	@Autowired
	private IngredientVariantRepository variantRepository;
	
	@Override
	public RecipeDto createRecipe()
	{
		final Recipe entity = new Recipe();
		entity.setName("Recipe Name");
		entity.setServes((short) 4);
		entity.setPrepTime(LocalTime.of(0, 0));
		entity.setCookingTime(LocalTime.of(0, 0));
		entity.setDescription("Ipsum Lorem");
		entity.setInstructions(new HashSet<>());
		
		return fetch(recipeRepository.save(entity));
	}
	
	@Override
	public void updateRecipe(final RecipeDto dto)
	{
		final Recipe entity = recipeRepository.getOne(dto.getId());
		entity.setName(dto.getName());
		entity.setServes(dto.getServes());
		entity.setPrepTime(dto.getPrepTime());
		entity.setCookingTime(dto.getCookingTime());
		entity.setDescription(dto.getDescription());
		entity.setIngredients(dto.getVariants()
				.stream()
				.map(d -> deserializeVariant(d))
				.collect(Collectors.toSet()));
		entity.setInstructions(dto.getInstructions()
				.stream()
				.map(d -> d.getText())
				.collect(Collectors.toSet()));
		
		recipeRepository.save(entity);
	}
	
	@Override
	public boolean deleteRecipe(long id)
	{
		final Optional<Recipe> recipeOpt = recipeRepository.findById(id);
		if (!recipeOpt.isPresent())
		{
			return false;
		}
		
		recipeRepository.delete(recipeOpt.get());
		return true;
	}
	
	@Override
	public RecipeDto fetchOne(long id)
	{
		final Recipe recipe = recipeRepository.getOne(id);
		if (recipe == null)
		{
			return null;
		}
		
		return fetch(recipe);
	}
	
	@Override
	public List<RecipeDto> fetchAll()
	{
		final List<Recipe> recipes = recipeRepository.findAll();
		final List<RecipeDto> result = new ArrayList<>();
		
		for (final Recipe recipe : recipes)
		{
			result.add(fetch(recipe));
		}
		
		return result;
	}
	
	
	private RecipeDto fetch(Recipe entity)
	{
		final RecipeDto dto = new RecipeDto(entity.getId(), entity.getName());
		dto.setServes(entity.getServes());
		dto.setPrepTime(entity.getPrepTime());
		dto.setCookingTime(entity.getCookingTime());
		dto.setDescription(entity.getDescription());
		dto.setVariants(entity.getIngredients()
				.stream()
				.map(e -> {
					final IngredientVariantDto result = new IngredientVariantDto(e.getId());
					final Ingredient ingredient = e.getIngredient();
					result.setIngredient(new IngredientDto(ingredient.getId(), ingredient.getName()));
					result.setQuantity(e.getQuantity());
					result.setUnit(e.getUnit());
					return result;
				})
				.collect(Collectors.toList()));
		dto.setInstructions(entity.getInstructions()
				.stream()
				.map(text -> new InstructionDto(text))
				.collect(Collectors.toList()));
		
		return dto;
	}
	
	private IngredientVariant deserializeVariant(final IngredientVariantDto dto)
	{
		IngredientVariant variant, storedVariant = null;
		
		if (dto.getId() > 0)
		{
			final Optional<IngredientVariant> opt = variantRepository.findById(dto.getId());
			if (!opt.isPresent())
			{
				return null;
			}
			
			// @TODO: Validate client request: Check if the requested variant belongs to this recipe.
			// Currently the client is able override variants belong to any recipe in the database.
			variant = storedVariant = opt.get();
		}
		else
		{
			variant = new IngredientVariant();
		}
		
		final IngredientDto ingredient = getOrCreateIngredient(dto.getIngredient());
		if (ingredient != null)
		{
			variant = updateVariant(variant, ingredient, dto);
			if (variant != null)
			{
				return variant;
			}
		}
		
		return storedVariant;
	}
	
	private IngredientVariant updateVariant(final IngredientVariant entity, final IngredientDto ingredient, final IngredientVariantDto dto)
	{
		entity.setIngredient(ingredient.getId());
		entity.setQuantity(dto.getQuantity());
		entity.setUnit(dto.getUnit());
		
		return variantRepository.save(entity);
	}
	
	private IngredientDto getOrCreateIngredient(final IngredientDto ingredient)
	{
		if (ingredient.getId() < 1)
		{
			final String name = ingredient.getName();
			if (name == null || name.isEmpty())
			{
				return null;
			}
			
			return ingredientService.createNew(ingredient);
		}
		
		return ingredient;
	}
}
