Vue.component('auto-complete', {
	template: `
		<div class="autocomplete">
			<input class="form-control"
					type="text"
					placeholder="Search"
					v-model="input"
					@keydown="on_key_down"
					@keyup.enter.prevent="0">
			<ul :class="{ 'active': is_active }">
				<li href="#"
						v-for="(item, i) in matched"
						v-bind:class="{ 'selected': selected_index == i }"
						@click="complete(i)">
					{{ item[field] }}
				</li>
			</ul>
		</div>`,
	props: {
		value: { type: String, required: false },
		data:  { type: Array, required: true },
		field: { type: String, required: true }
	},
	created: function() {
		this.input = this.value || ''
	},
	updated: function() {
		this.$emit('input', this.input);
	},
	methods: {
		complete(i) {
			var matched = this.matched;
			this.input = matched[i][this.field];
			this.is_active = false;
			this.$emit('completed', matched[i]);
		},
		on_key_down: function(event) {
			if (event.keyCode == 9) { // tab
				if (!this.is_active || this.matched.length < 1) {
					return;
				}
				
				this.complete(0);
			} else if (event.keyCode == 13) { // enter
				if (this.selected_index !== -1) {
					this.complete(this.selected_index);
					this.selected_index = -1;
				}
			} else if (event.keyCode == 38) { // up arrow
				if (this.selected_index > 0) {
					this.selected_index--;
				}
			} else if (event.keyCode == 40) { // down arrow
				if (this.selected_index < this.data.length - 1) {
					this.selected_index++;
				}
			} else {
				this.selected_index = -1;
				this.is_active = true;
				this.$emit('changed', this.input);
				return;
			}
			
			event.preventDefault();
		},
		is_matching_input: function(item) {
			return item[this.field].toLowerCase().indexOf(this.input.toLowerCase()) != -1;
		}
	},
	computed: {
		matched: function() {
			var result = [];
			for (var i = 0; i < this.data.length; ++i) {
				if (this.is_matching_input(this.data[i])) {
					result.push(this.data[i]);
				}
			}
			
			return result;
		}
	},
	data: function() {
		return {
			input: '',
			is_active: false,
			selected_index: -1
		}
	}
});
