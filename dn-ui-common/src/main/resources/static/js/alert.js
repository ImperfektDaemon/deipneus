const Alert = {
	props: ['category', 'text'],
	template: `
			<transition name="fade">
				<div class="alert-message" v-if="show">
					<div :class="'alert alert-' + category">
						{{ text }}
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" v-on:click="show = false">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			</transition>`,
	mounted: function() {
		var self = this;
		setTimeout(function() {
			self.show = false;
		}, 3000)
	},
	data: function() {
		return {
			show: true
		};
	}
};

const AlertCtor = Vue.extend(Alert);

show_alert_message = function(category, text) {
	const alert = new AlertCtor({
		propsData: {
			category: category,
			text: text
		}
	}).$mount();
	
	document.getElementById('alert-messages').appendChild(alert.$el);
};