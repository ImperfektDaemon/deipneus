_has_response = function(status ) {
	return status == 200
			|| status == 400;
};

ajax_request = function(method, url, data, callback) {
	console.warn("ajax_request is deprecated, consider to ajax_call or rest_call");
	
	ajax_call(method, url, {
				"Content-Type": "application/json"
			}, data, callback);
};

ajax_call = function(method, url, headers, data, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
        	var status = xhr.status;
        	var response = undefined;
        	if (_has_response(status)) {
        		response = xhr.responseText;
        	}
        	
            callback(status, response);
        }
    }
    
    xhr.open(method, url, true);
    for (var key in headers) {
        xhr.setRequestHeader(key, headers[key]);
    }
    xhr.send(data);
};

rest_call = function(method, url, data, callback) {
	var payload = data;
	if (typeof data === 'object') {
		payload = JSON.stringify(data);
	}
	
	ajax_call(method, url, {
				"Content-Type": "application/json"
			}, payload, function(status, response) {
				if (response !== undefined) {
					response = JSON.parse(response);
				}
				
				callback(status, response);
			});
}