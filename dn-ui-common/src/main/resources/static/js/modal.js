Vue.component('modal', {
	template: `
		<div class="modal">
		</div>`,
	props: {
		value: { type: String, required: false },
		data:  { type: Array, required: true },
		field: { type: String, required: true }
	},
	methods: {
	},
	data: function() {
		return {
		};
	}
});
