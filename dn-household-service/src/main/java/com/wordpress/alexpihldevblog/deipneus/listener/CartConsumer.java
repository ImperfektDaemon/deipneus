package com.wordpress.alexpihldevblog.deipneus.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.wordpress.alexpihldevblog.deipneus.message.CartCompletionMessage;
import com.wordpress.alexpihldevblog.deipneus.service.MealService;

@Component
public class CartConsumer
{
	
	@Autowired
	private MealService mealService;
	
	@KafkaListener(topics = { "deipneus.carts" })
	private void consume(CartCompletionMessage message)
	{
		mealService.createNew();
	}
}
