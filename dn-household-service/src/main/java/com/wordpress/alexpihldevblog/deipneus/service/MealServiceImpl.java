package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wordpress.alexpihldevblog.deipneus.dto.MealDto;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.Meal;
import com.wordpress.alexpihldevblog.deipneus.persistence.repository.MealRepository;

@Service
@Transactional
public class MealServiceImpl implements MealService
{
	
	@Autowired
	private MealRepository repository;
	
	@Override
	public MealDto createNew()
	{
		return fetch(repository.save(new Meal()));
	}
	
	@Override
	public List<MealDto> fetchAll()
	{
		final List<MealDto> result = new ArrayList<>();
		final List<Meal> entities = repository.findAll();
		for (final Meal entity : entities)
		{
			result.add(fetch(entity));
		}
		
		return result;
	}
	
	
	private MealDto fetch(final Meal entity)
	{
		return new MealDto(entity.getId());
	}
}
