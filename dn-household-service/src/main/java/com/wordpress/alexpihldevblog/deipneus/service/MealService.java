package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.List;

import com.wordpress.alexpihldevblog.deipneus.dto.MealDto;

public interface MealService
{
	MealDto createNew();
	
	List<MealDto> fetchAll();
}
