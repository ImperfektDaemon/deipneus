package com.wordpress.alexpihldevblog.deipneus.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wordpress.alexpihldevblog.deipneus.service.MealService;

@Controller
public class WebMealController
{
	
	@Autowired
	private MealService service;
	
	@RequestMapping(value = "/meals", method = RequestMethod.GET)
	private ModelAndView getAllMeals()
	{
		final ModelAndView mav = new ModelAndView("meal/meal-list");
		mav.addObject("meals", service.fetchAll());
		return mav;
	}
}
