package com.wordpress.alexpihldevblog.deipneus.message;

public class CartCompletionMessage
{
	
	private long cartId;
	
	public CartCompletionMessage()
	{
		this(0);
	}
	
	public CartCompletionMessage(long cartId)
	{
		this.cartId = cartId;
	}
	
	public void setCartId(long value)
	{
		cartId = value;
	}
	
	public long getCartId()
	{
		return cartId;
	}
}
