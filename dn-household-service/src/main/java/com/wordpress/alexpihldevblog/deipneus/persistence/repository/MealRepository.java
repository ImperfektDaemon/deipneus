package com.wordpress.alexpihldevblog.deipneus.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wordpress.alexpihldevblog.deipneus.persistence.model.Meal;

public interface MealRepository extends JpaRepository<Meal, Long>
{
}
