package com.wordpress.alexpihldevblog.deipneus.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	private ModelAndView showShoppingList()
	{
		final ModelAndView mav = new ModelAndView("index");
		return mav;
	}
}
