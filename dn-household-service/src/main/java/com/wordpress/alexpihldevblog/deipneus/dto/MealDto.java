package com.wordpress.alexpihldevblog.deipneus.dto;

public class MealDto
{
	
	private long id;
	
	public MealDto(long id)
	{
		this.id = id;
	}
	
	public long getId()
	{
		return id;
	}
}
