package com.wordpress.alexpihldevblog.deipneus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
			"com.wordpress.alexpihldevblog.deipneus.web",
			"com.wordpress.alexpihldevblog.deipneus.api",
			"com.wordpress.alexpihldevblog.deipneus.config",
			"com.wordpress.alexpihldevblog.deipneus.listener",
			"com.wordpress.alexpihldevblog.deipneus.service"
		})
@EnableEurekaClient
public class HouseholdApplication {

	public static void main(String[] args) {
		SpringApplication.run(HouseholdApplication.class, args);
	}
}
