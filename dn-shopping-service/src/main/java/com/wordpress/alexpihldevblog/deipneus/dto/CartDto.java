package com.wordpress.alexpihldevblog.deipneus.dto;

public class CartDto
{
	
	private long id;
	
	private String name;
	
	public CartDto(long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getId()
	{
		return id;
	}
}
