package com.wordpress.alexpihldevblog.deipneus.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wordpress.alexpihldevblog.deipneus.persistence.model.Cart;

public interface CartRepository extends JpaRepository<Cart, Long>
{
}
