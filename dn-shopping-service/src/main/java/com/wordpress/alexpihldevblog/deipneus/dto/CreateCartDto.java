package com.wordpress.alexpihldevblog.deipneus.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateCartDto
{
	@JsonProperty(value = "recipe_id", required = true)
	private long recipeId;
	
	@JsonAlias({ "recipe_id" })
	public void setRecipeId(long value)
	{
		recipeId = value;
	}
	
	public long getRecipeId()
	{
		return recipeId;
	}
}
