package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.List;

import com.wordpress.alexpihldevblog.deipneus.dto.CartDto;

public interface CartService
{
	CartDto createCart(long recipeId);
	
	boolean completeById(long id);
	
	boolean deleteById(long id);
	
	List<CartDto> fetchAll();
}
