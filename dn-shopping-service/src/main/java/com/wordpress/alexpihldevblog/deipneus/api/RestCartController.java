package com.wordpress.alexpihldevblog.deipneus.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wordpress.alexpihldevblog.deipneus.dto.CartDto;
import com.wordpress.alexpihldevblog.deipneus.dto.CreateCartDto;
import com.wordpress.alexpihldevblog.deipneus.service.CartService;

@RestController
@RequestMapping(
		path = "/api/1.0",
		consumes = { MediaType.APPLICATION_JSON_VALUE },
		produces = { MediaType.APPLICATION_JSON_VALUE })
public class RestCartController
{
	private static class CartRef
	{
		@JsonProperty(value = "id", required = true)
		public long id;
	}
	
	@Autowired
	private CartService service;
	
	@RequestMapping(value = "/carts", method = RequestMethod.GET)
	private ResponseEntity<List<CartDto>> getAllCarts()
	{
		final List<CartDto> result = new ArrayList<>();
		
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/carts", method = RequestMethod.POST)
	private ResponseEntity<String> createCart(@Valid @RequestBody final CreateCartDto request, final Errors errors)
	{
		if (errors.hasErrors())
		{
			return ResponseEntity.badRequest().body("{}");
		}
		
		service.createCart(request.getRecipeId());
		
		return ResponseEntity.ok("{}");
	}
	
	@RequestMapping(value = "/carts", method = RequestMethod.PUT)
	private ResponseEntity<String> completeCart(@Valid @RequestBody final CartRef request, final Errors errors)
	{
		if (errors.hasErrors())
		{
			return ResponseEntity.badRequest().body("{}");
		}
		
		if (!service.completeById(request.id))
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok("{}");
	}
	
	@RequestMapping(value = "/carts", method = RequestMethod.DELETE)
	private ResponseEntity<String> cancelCart(@Valid @RequestBody final CartRef request, final Errors errors)
	{
		if (errors.hasErrors())
		{
			return ResponseEntity.badRequest().body("{}");
		}
		
		if (!service.deleteById(request.id))
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok("{}");
	}
}
