package com.wordpress.alexpihldevblog.deipneus.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wordpress.alexpihldevblog.deipneus.dto.CartDto;
import com.wordpress.alexpihldevblog.deipneus.message.CartCompletionMessage;
import com.wordpress.alexpihldevblog.deipneus.persistence.model.Cart;
import com.wordpress.alexpihldevblog.deipneus.persistence.repository.CartRepository;

@Service
@Transactional
public class CartServiceImpl implements CartService
{
	
	private final static String TOPIC = "deipneus.carts";
	
	@Autowired
	private KafkaTemplate<String, CartCompletionMessage> kafkaTemplate;
	
	@Autowired
	private CartRepository repository;
	
	@Override
	public CartDto createCart(long recipeId)
	{
		final Cart newCart = new Cart();
		newCart.setRecipeId(recipeId);

		return fetch(repository.save(newCart));
	}
	
	@Override
	public boolean completeById(long id)
	{
		kafkaTemplate.send(TOPIC, new CartCompletionMessage(id));
		return true;
	}
	
	@Override
	public boolean deleteById(long id)
	{
		final Optional<Cart> cartOpt = repository.findById(id);
		if (!cartOpt.isPresent())
		{
			return false;
		}
		
		repository.delete(cartOpt.get());
		return true;
	}
	
	@Override
	public List<CartDto> fetchAll()
	{
		final List<CartDto> result = new ArrayList<>();
		final List<Cart> carts = repository.findAll();
		for (Cart cart : carts)
		{
			result.add(fetch(cart));
		}
		
		return result;
	}
	
	
	private CartDto fetch(Cart cart)
	{
		final CartDto dto = new CartDto(cart.getId(), String.format("#%s", cart.getId()));
		return dto;
	}
}
