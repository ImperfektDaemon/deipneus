package com.wordpress.alexpihldevblog.deipneus.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cart")
public class Cart
{

	@Id
	@Column(unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	private long recipeId;
	
	public Cart()
	{ }
	
	public void setRecipeId(long value)
	{
		recipeId = value;
	}
	
	public long getRecipeId()
	{
		return recipeId;
	}
	
	public long getId()
	{
		return id;
	}
}
