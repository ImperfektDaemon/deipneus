package com.wordpress.alexpihldevblog.deipneus.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wordpress.alexpihldevblog.deipneus.service.CartService;

@Controller
public class HomeController
{
	
	@Autowired
	private CartService service;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	private ModelAndView showShoppingList()
	{
		final ModelAndView mav = new ModelAndView("index");
		return mav;
	}
	
	@RequestMapping(value = "/carts", method = RequestMethod.GET)
	private ModelAndView showAllCarts()
	{
		final ModelAndView mav = new ModelAndView("cart/cart-list");
		mav.addObject("carts", service.fetchAll());
		
		return mav;
	}
	
	@RequestMapping(value = "/carts/{id}", method = RequestMethod.GET)
	private ModelAndView showCart(@PathVariable("id") final Long id)
	{
		final ModelAndView mav = new ModelAndView("cart/cart");
		return mav;
	}
}
